import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;


public class RpsApplication extends Application {
	private RpsGame game;	
	public void start(Stage stage) {
		Group root = new Group(); 

		//scene is associated with container, dimensions
		Scene scene = new Scene(root, 650, 300); 
		scene.setFill(Color.BLACK);
		//associate scene to stage and show
		stage.setTitle("Rock Paper Scissors"); 
		stage.setScene(scene); 
		VBox overall =new VBox();
		HBox buttons = new HBox();
		Button rockbtn = new Button("Rock ");
		Button paperbtn = new Button("Paper");
		Button scissorbtn= new Button("Scissors");
		buttons.getChildren().addAll(rockbtn, paperbtn,scissorbtn);
		overall.getChildren().add(buttons);
		HBox textfields = new HBox();
		TextField welcome =new TextField("Welcome!");
		welcome.setPrefWidth(200);
		TextField wins= new TextField("Wins: ");
		wins.setPrefWidth(200);
		TextField losses=new TextField("Losses: ");
		losses.setPrefWidth(200);
		TextField ties= new TextField("Ties: ");
		ties.setPrefWidth(200);
		textfields.getChildren().addAll(welcome,wins,losses,ties);
		overall.getChildren().addAll(textfields);
		root.getChildren().add(overall);

		stage.show(); 
	}
	
    public static void main(String[] args) {
        Application.launch(args);
    }
}    

