import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
public class RpsChoice implements EventHandler<ActionEvent> {
    //Six fields for the display later
    private String message;
    private int winCount;
    private int loseCount;
    private int tieCount;
    private String userChoice;
    private RpsGame game;
    //Constructor
    public RpsChoice(String message,int wins,int losses,int ties,String userChoice,RpsGame game){
        this.message=message;
        this.winCount=wins;
        this.loseCount=losses;
        this.tieCount=ties;
        this.userChoice=userChoice;
        this.game= new RpsGame();
    }
    @Override
    public void handle(ActionEvent arg0) {
        String result=this.game.playRound(this.userChoice);
    }
    
}
