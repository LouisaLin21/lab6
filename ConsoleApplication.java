import java.util.*;
public class ConsoleApplication {
   public static void main(String[]args){
    Scanner scan = new Scanner(System.in);
    RpsGame r = new RpsGame();
    System.out.println("Please enter your choice");
    String choice=scan.nextLine();
    String res= r.playRound(choice);
    System.out.println(res);

   }
}
