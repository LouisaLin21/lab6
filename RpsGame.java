import java.util.*;
public class RpsGame{
    private int numWins;
    private int numTies;
    private int numLosses;
    private Random ran;

    public RpsGame(){
        this.numWins=0;
        this.numTies=0;
        this.numLosses=0;
        this.ran=new Random();
    }
    public int getWins(){
        return this.numWins;

    }
    public int getTies(){
        return this.numTies;

    }
    public int getLosses(){
        return this.numLosses;

    }
    public String playRound(String choice){
        String[] translate = {"rock","scissors","paper"};
        int computerChoice=ran.nextInt(3);
        String computerChoiceString="";
        if(computerChoice==0){
            computerChoiceString=translate[0];
        }
        else if(computerChoice==1){
            computerChoiceString=translate[1];
        }
        else{
            computerChoiceString=translate[2];
        }
        String winner="";
        /*
        *Tie situation
         */
        if(choice.equals(computerChoiceString)){
            winner="nobody";
        }
        /*
        *Situations where the computer wins
         */
        else if(choice.equals("rock")&&computerChoiceString.equals("scissors")|| choice.equals("scissors")&&computerChoiceString.equals("paper")||choice.equals("paper")&&computerChoiceString.equals("rock")){
            winner="computer";
        }
        /*Else the user wins */
        else{
            winner="user";
        }

        return "Computer played "+computerChoiceString+" and "+winner+" won";
    }
}